Project Name:  **KiCon-Shenzhen-2023**  
Project Date:  _2023-07-17_ 
  
Earlier in the year Torrey suggested we make up a SAO hat while in Shenzhen.  It was added as a workshop for the KiCAD Asian November 2023 conference.  
Since then I have added a few extra SAO boards and two kinds of Controllers/Tester 'Badges'  This has been a fun project.
  
 
**Specification URLs:  
------------------**  
 
* https://hackaday.io/project/175182-simple-add-ons-sao  
* https://hackaday.com/2019/03/20/introducing-the-shitty-add-on-v1-69bis-standard/  
  
**Connectors:  
-----------**  
 
* https://www.aliexpress.com/item/33060894189.html  // Female 2x3 with key  
* available at the Shenzhen HQB Electronics Markets  

**Raspberry Pico RP2040 Controller 'Badge' PCB Versions:  
---------------------------------**  

This is a cheap, small and quick SAO Badge controller to enable coding and testing of a SAO board. No Hot-swapping of a SAO board.  

**Version 1.1**  
Production:  NextPCB, 6 Layer, 1.6mm, Red, ??x??mm, HASL, 200pcs  
Date: 2024-10-24  

Done: added HQ and kiCad logos  
Done: added serial number block  
Done: updated version numbers  
Done: updated routing around R10  


**Version 1.0**  
Production:  JLCPCB, 6 Layer, 1.6mm, Purple, ??x??mm, ENIG  
Date: 2024-09-12  

Done: 3rd version of the design. To be used in SAO workshops.  
Done: based on a RP2040 MCU.  

 
**ESP32-C3 SAO Tester 'Badge' PCB Versions:  
---------------------------------**  

This is a SAO Tester. It can measure the voltage and current flow through a SAO board. It allows hot-swapping and running up to two SAO boards.  
 
**Version 1.4**  
Production:  JLCPCB, 6 Layer, 1.6mm, Green, 78x87mm, ENIG  
Date: 2023-12-08  
 
Done: add a i2C Mux IC: PCA9546 i2C bus multiplxer to add in hot-swapping SAO  
Done: add an extra LDO to power the SAO ports  
Done: Add a MOSFET to each SAO Port so they can be isolated.  We can then read one i2C EEPROM at a time!  
Done: add a 100m Ohm 1% 1206 sense resistor + INA219 to measure current.  This will allow some kind of SAO power monitoring  
 
 
**Version 1.3**  
Production:  JLCPCB, 2 Layer, 1.6mm, White, 76x87mm  
Date: 2023-10-27  
 
Done: add a 'Made with kiCAD' logo on the backside  
Done: remove one SAO port. Need the GPIO  
Done: Add a VCC highside MOSFET switch to isolate each SAO port.  I can only read one i2C EEPROM at a time!  
No: add a 100m Ohm 0.1% 1206 sense resistor + INA219 to measure current.  This will allow some kind of SAO power monitoring.  
 
 
**Version 1.2**  
Production:  JLCPCB, 2 Layer, 1.6mm, White, 76x87mm  
Date: 2023-09-14  
 
Done: add SAO Port number to backside as well - move data into to relevant SAO  
Done: add a PCB version number to backside  
Done: maybe rotate the connectors?  Or maybe one or two of them?  
Done: change the SAO2 to be a full (normal) connector - if there are enough spare GPIO?  
Done: remove SW4  
Done: add a [Reset] text box to Reset switch (move cap text further away)  
Done: add test point labels to back of PCB:  3V3 + GND  
Done: maybe add i2C testpoints close to PCB edge? and label  
Done: add a 'M3' text on backside near mounting hole  
Done: added i2C pullup resistors  
Done: changed resistor footprints from capacitors to resistors  
Done: rounded out tracks with plugin  
 
 
**Version 1.1**  
Production:  JLCPCB, 2 Layer, 1.6mm, Green, 76x87mm  
Date: 2023-07-21  
  
Done: add a second thru-hole connector for each SAO connector. This will allow the choice of two types (SMD/THT) to use in assembly  
Done: fix the 6-pin connector. It was on around the wrong way around  
  
 
**Version 1.0**  
Production:  JLCPCB, 2 Layer, 1.6mm, Green, 76x87mm  
Date: 2023-07-19  
 
* initial PCB design sent off  
* 3 SAO connectors, with one being a special switch power connector  
* ESP32-C3 based design  
 
 
 
**SAO Example PCB Versions:  
------------------------**  
 
In future add a SOT23-5 i2C based EEPROM memory IC (A$0.07/ea) so I can store some kind of ID on the board so the Tester knows which program to run and what GPIO to use, number of RGB LEDs etc!  
 
 
**Version 1.0: KiCon SAO**  
Production:  JLCPCB, 2 Layer, 1.0mm, Blue, 50mm x 35.5mm, 20pcs  
Date: 2023-09-14   
 
Done: add a SOT23-5 i2c EEPROM - C84964  K24C02, + Write Protect jumper  
 
 
**Version 0.1: KiCon SAO**  
Production:  JLCPCB, 2 Layer, 1.0mm, Blue, 50mm x 35.5mm, 5 panels of 20 = 100pcs  
Date: 2023-07-21  
 
* initial design  
 
 
 




