# Simple Add On (SAO) KiCon Asia 2024
  
This is a small PCB to help demonstrate SMT component assembly
For more documentation visit: [https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs/-/tree/main/PCB/SAO_KiCon-Asia-2024](https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs/-/tree/main/PCB/SAO_KiCon-Asia-2024)
  
## Hardware Features:
---------
#### Version 1.1 Features:

* single 2mm RGB LED that shines through PCB from the reverse side
* single 1206 sized reverse mounted Red LED
* SAO 6 pin SMD 2.54mm connector
* 0805 0 ohm resistor to demonstrate different component sizes and jumping tracks
* ATC2402 256 byte EEPROM for board identification
* 2 x 100nF 0603 sized decoupling capacitors
* 1 x 2K ohm current limiting resistor
* 2 layer PCB, with no via's to keep the front layer clean


## Board Images:
![SAO KiCon Asia 2024 - Front](images/KiCon-Asia-24_Front.png "Front"){ align=left : style="height:150px;width:145px"}
![SAO KiCon Asia 2024 - Back](images/KiCon-Asia-24_Back.png "Back"){ align=left : style="height:150px;width:145px"}
<br><br><br><br><br><br><br><br><br><br><br>

  
## Available Software 
---------------

![Gitlab KiCon SAO Project Code URL](images/SAO_KiCon_2024-code-URL.png "Code URL"){ align=right : style="height:130px;width:130px"}



#### Example Arduino Code:
* `Example Code`  Test code is designed to be installed onto the SAO Controller board via the Arduino IDE:
  


