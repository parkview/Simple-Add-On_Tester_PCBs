# Simple Add On (SAO) Controller - KiCon Asia 2024
  
This PCB acts as a 'Badge' to allow different SAO boards to be plugged in, identify the SAO board and run the relevant code for that SAO board.

For more documentation visit: [https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs/-/tree/main](https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs/-/tree/main)
  
## Hardware Features:
---------
#### Version 1.1 Features:

* Raspberry Pi Pico RP2040 Controller
* one SAO 6 pin socket connector 2.54mm (0.1") pitch (3V3, GND, i2C SDA, i2C CLK, GPIO10, GPIO7)
* User tactile switch
* two 0402 LEDs, one acting as a programmable Power White LED, the other as a User Orange LED
* USB C for power and programming interface
* seven pin 1.27mm (0.05") pitch breakout for end-user use
* Onboard GPIO documentation


## Board Images:
![SAO Controller KiCon Asia 2024 - Front](images/SAO_Controller_v1.1_Front_sml.jpg "Controller Front"){ align=left : style="height:220px;width:145px"}
![SAO Controller KiCon Asia 2024 - Back](images/SAO_Controller_v1.1_Back_sml.jpg "Controller Back"){ align=left : style="height:220px;width:145px"}
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


## Sponsor
This PCB has been sponsored and produced by NextPCB

## Available Software
---------------

![Gitlab KiCon SAO Controller Project Code URL](images/SAO_KiCon_2024-code-URL.png "https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs/-/tree/main/CODE/RP2040-SAO_Controller_v0.2/Arduino-IDE/SAO-RP2040-Controller_v1.1"){ align=right : style="height:130px;width:130px"}



#### Example Arduino Code:
* `Example Code`  Test code is designed to be installed onto the SAO Controller board via the Arduino IDE:
  


