/*
 *
 *   ESP32-C3 Simple-Add-On (SAO) Tester Board v1.4
 *
 * Parkview 2023-08-07
 *
 * v1.4 has a PCA9546 i2C bus switch to help with semi hot-swaping SAO boards + INA219 to measure current - green board
 * v1.3 has switched Dual SAO Ports - white board
 * v1.2 has 3 multiuse SAO ports - white board
 * V1.1 has 2 multiuse SAO ports and 1 switched port port (no i2C) - purple board
 *
 * Gitlab Project URL: https://gitlab.com/parkview/ESP32-C3_SAO-Tester
 *
 * MIT License
 *
 *
 * Example EEPROM ID Code: C003LR1,Kicon
 *  byte 0 = hex bytes (length) of ID string
 *  003 = ID of SAO Board, in this case, it's the KiCon SAO
 *  L = Plain LED in the left hand side pin (Pin 5?)
 *  R = RGB LED in the right hand side pin (Pin 6)
 *  1 = hex number (0-15)of RGB LEDs in the string
 *  , = deliniates that text is next
 *  text = human name of SAO board
 *
 * Note: could have some kind of info on i2C IC on SAO board? like a MCU, or GPIO expander?
 *
 * TODO List:
 *
 * add in code for a 2nd mode - do something different?
 * need to write code to switch power to each SAO port and read the boards EEPROM
 * add in WiFi AP mode + display current SAO Hat + power usage (for v1.4 PCB)
 * could move code to FreeRTOS event mode?
 * Done: run default no-code for the SZ-Asia-KiCon SAO board
 * could store the Tester board version (ie: 1.2, 1.3, 1.4, 1.5 etc) in ESP32-C3 flash so the code knows what features the board can accomplish
 *
 *  ### NOTE: need to turn on relevant SAO Port before talking to the EEPROM. Need code for there  ####
 *            Maybe put the SAO Port GPIO for pins 5+6 into an array or Struct to make it easier address the SAO port.
 *
 */

#include <Arduino.h>
#include "FastLED.h"
#include <EEPROM.h>
#include <Wire.h>
#include "TCA9548.h" // from library: robtillaart/TCA9548  - used to select a i2C Port

const u8_t LED1 = 8;          // GPIO of the onboard Green D1
const u8_t LED2 = 2;          // GPIO of the onboard Blue D2
const u8_t SAO1_P5 = 10;      // SZ23 Red LED
const u8_t SAO1_P6 = 7;       // SZ23 RGB LED
const u8_t PowerMOSFET1 = 20; // GPIO of the SAO1 MOSFET switch
const u8_t PowerMOSFET2 = 21; // GPIO of the SAO2 MOSFET switch
const u8_t SAO2_P5 = 05;      // SAO2 - pin 5
const u8_t SAO2_P6 = 04;      // SAO2 - pin 6
const u8_t RST_pin = 3;       // GPIO of the i2C Bus Reset line. Must be held high to operate. Low to reset i2C bus

const u8_t sw1Pin = 6;                // SW1 - Mode press = 0
const u8_t sw2Pin = 9;                // SW2 - Boot press = 0
const u8_t EEPROM_I2C_ADDRESS = 0x50; // EEPROM = 80D
const u8_t SDA_pin = 0;               // i2C Data GPIO
const u8_t SCL_pin = 1;               // i2C Clock GPIO
u8_t address = 0;                     // initial EEPROM Address. Examples: value = EEPROM.read(address); EEPROM.update(address, val); EEPROM.length())

#define numRGBLED 4    // number of RGB LEDs to light up
#define BRIGHTNESS 250 /* Control the brightness of your RGB leds */
#define SATURATION 255 /* Control the saturation of your RGB leds */
#define WAIT 100
#define DELAYVAL 50

const uint8_t ledChannel0 = 0; // onboard Green LED
const uint8_t ledChannel1 = 1; // not currently used
const uint8_t ledChannel2 = 2; // onboard Blue LED
const uint8_t ledChannel3 = 3; // SAO Port 1 plain LED, on my boards
const uint8_t ledChannel4 = 4; // SAO Port 2 plain LED, on my boards
const int freq = 5000;

const uint8_t resolution = 12;            // PWM resolution
bool sw1Press = false;                    // tracks SW1 button interrupt
bool sw2Press = false;                    // tracks SW2 button interrupt
bool wifi = false;                        // tracks wifi status, on/off
uint8_t LEDmode = 0;                      // tracks the current LED mode, start at zero
uint8_t LEDmodeOld = 0;                   // tracks the last LED mode, start at zero
uint16_t loopCount = 0;                   // tracks how many times we have looped around the Loop function
uint16_t greenBrightness = 500;           // how bright should the Green Tester LED be:  0 - 4096
bool toggleLED = 0;                       // ?? used?
bool skip_i2C = false;                    // ignore i2C if no device is found
const u8_t maxLEDs = 15;                  // due to the Power 133mA limit, this limits the max number of RGB LEDs we can use. Really it should be around 2(2 x 60mA = 120mA!) at full brightness
uint8_t channels = 0;                     // PCA9546 Channel number. Note: v1.4 only uses Port 0 and Port 1. Other two ports are un used
u8_t num_led[] = {0, 0};                  // tracks the number of RGB LEDs on a SAO Hat (read from EEPROM) on that SAO Port
u8_t activePort[2][2] = {{0, 0}, {0, 0}}; // tracks if a SAO port is in use or not, and the type of SAO (Target) that installed on it
u8_t target = 0;                          // records the type of SAO Hat.

enum SAOType
{
  NONE,
  KOALA,
  TROUBLEMAKER,
  KICON23
}; // tracks the type of SAO board it is - u8_t = 0,1,2,3

PCA9546 MP(0x70); // i2C address
CRGB leds[maxLEDs];
void writeEEPROM(u8_t address, u8_t val);
u8_t readEEPROM(u8_t address);

void rainbow_wave(uint8_t waveBrightness)
{
  for (int j = 0; j < 255; j++)
  {
    {
      leds[0] = CHSV(0 - (j * 2), SATURATION, waveBrightness); /* The higher the value 4 the less fade there is and vice versa */
    }
    FastLED.show();
    delay(25); /* Change this to your hearts desire, the lower the value the faster your colors move (and vice versa) */
  }
}

void rainbow(uint8_t thisSpeed, uint8_t deltaHue)
{                                                   // The fill_rainbow call doesn't support brightness levels.
  uint8_t thisHue = beatsin8(thisSpeed, 0, 255);    // A simple rainbow wave.
  fill_rainbow(leds, numRGBLED, thisHue, deltaHue); // Use FastLED's fill_rainbow routine.
} // rainbow_wave

void twinkle()
{
  if (sw1Press)
  {
    // Mode button has been pressed
    return;
  }
  // randomly twinkle some LEDs
  int i = random(numRGBLED); // A random number. Higher number => fewer twinkles. Use random16() for values >255.
  if (i < numRGBLED)
    leds[i] = CHSV(random(255), random(255), random(255)); // Only the lowest probability twinkles will do. You could even randomize the hue/saturation. .
  for (int j = 0; j < numRGBLED; j++)
    leds[j].fadeToBlackBy(8);
  LEDS.show(); // Standard FastLED display
  LEDS.delay(10);
}

void addGlitter(fract8 chanceOfGlitter)
{
  if (random8() < chanceOfGlitter)
  {
    leds[random16(numRGBLED)] += CRGB::White;
  }
}

void blinkUserLED(uint16_t PWMvalue)
{
  // blink the User LEDs, Red, then Green
  ledcWrite(ledChannel1, PWMvalue);
  delay(WAIT * 4);
  ledcWrite(ledChannel1, 0);
  delay(WAIT * 3);
  ledcWrite(ledChannel2, PWMvalue);
  delay(WAIT * 4);
  ledcWrite(ledChannel2, 0);
}

void blinkUserLEDGreen(uint16_t PWMvalue)
{
  // blink both the Green User LEDs
  ledcWrite(ledChannel0, PWMvalue);
  delay(500);
  ledcWrite(ledChannel0, 0);
}

void blinkUserLEDBlue(uint16_t PWMvalue)
{
  // blink both the Red User LEDs
  // turn this into a task!
  ledcWrite(ledChannel2, PWMvalue);
  delay(100);
  ledcWrite(ledChannel2, 0);
}

void toggleGreen() // this needs more work!
{
  // toogle Green LED to oposite, ie: on/off
  if (loopCount % 100)
  {
    // we have looped nn times, so toggle the LED
    toggleLED = !toggleLED;
  }
  ledcWrite(ledChannel0, greenBrightness * toggleLED);
  loopCount++;
}

IRAM_ATTR void ISRsw1()
{
  // the button switch interrupt has been called
  sw1Press = true;
}

IRAM_ATTR void ISRsw2()
{
  // the button switch interrupt has been called
  sw2Press = true;
}

void writeStringToEEPROM(int addrOffset, const String &strToWrite)
{
  byte len = strToWrite.length();
  writeEEPROM(addrOffset, len);
  for (int i = 0; i < len; i++)
  {
    writeEEPROM(addrOffset + 1 + i, strToWrite[i]);
    Serial.println(strToWrite[i]);
  }
}

String readStringFromEEPROM(int addrOffset)
{
  int newStrLen = readEEPROM(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++)
  {
    data[i] = readEEPROM(addrOffset + 1 + i);
  }
  data[newStrLen] = '\0'; // !!! NOTE !!! Remove the space between the slash "/" and "0" (I've added a space because otherwise there is a display bug)
  return String(data);
}

// Function to write to EEPROOM
void writeEEPROM(u8_t address, u8_t val)
{
  // Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((u8_t)(address)); // AT24C02 only needs one byte address
  // Send data to be stored
  Wire.write(val);
  // End the transmission
  Wire.endTransmission();
  //  Add 5ms delay for EEPROM
  delay(5);
}

// Function to read from EEPROM
u8_t readEEPROM(u8_t address)
{
  // Define byte for received data
  byte rcvData = 0xFF;
  // Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((u8_t)(address)); // AT24C02 only needs one byte address
                               // End the transmission
                               // Serial.print("i2C Write: ");
  // Wire.endTransmission(); // should report back a 0 for success
  Wire.endTransmission(false); // should report back a 0 for success
  delayMicroseconds(40);
  //  Request one byte of data at current memory address
  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);
  // Read the data and assign to variable
  if (Wire.available())
  {
    rcvData = Wire.read();
  }
  delayMicroseconds(40);
  //  Return the data as function output
  return rcvData;
}

void setup()
{
  // const u8_t num_led = 0;
  //  Setup ESP32-S3 GPIOs:
  pinMode(sw1Pin, INPUT);
  pinMode(sw2Pin, INPUT);
  // pinMode(SAO1_P6, OUTPUT); // needed to enable RGB data output
  pinMode(PowerMOSFET1, OUTPUT);
  pinMode(PowerMOSFET2, OUTPUT);
  pinMode(RST_pin, OUTPUT);
  // Turn on SAO port 2 (high is OFF, low is ON):
  digitalWrite(PowerMOSFET1, HIGH);                       // turn off SAO port 1, use this one first.
  digitalWrite(PowerMOSFET2, LOW);                        // turn on SAO port 2
  digitalWrite(RST_pin, HIGH);                            // turn on i2C Bus Master IC
  FastLED.addLeds<SK6812, SAO2_P6, GRB>(leds, numRGBLED); // SK6812 (GRB) KiCon SZ23 and works with 6028RGB LEDs
  ledcSetup(ledChannel0, freq, resolution);
  ledcSetup(ledChannel1, freq, resolution);
  ledcSetup(ledChannel2, freq, resolution);
  ledcSetup(ledChannel3, freq, resolution);
  ledcSetup(ledChannel4, freq, resolution);
  ledcAttachPin(LED1, ledChannel0); // on board Power (Green) LED
  ledcAttachPin(LED2, ledChannel2); // on board Status (Blue) LED
  // Note Red LEDs below are currently hard coded. Ideally we should read the EEPROM to see what the SAO board needs
  ledcAttachPin(SAO1_P5, ledChannel3); // SAO port 1 Red LEDs
  ledcAttachPin(SAO2_P5, ledChannel4); // SAO port 2 Red LEDs
  blinkUserLED(3000);                  // just a test to make sure both onboard (Green+Blue) LEDs are working
  //  turn off the User leds
  ledcWrite(ledChannel0, 1000);
  ledcWrite(ledChannel1, 0);
  ledcWrite(ledChannel2, 0);
  ledcWrite(ledChannel3, 0);
  attachInterrupt(digitalPinToInterrupt(sw1Pin), ISRsw1, FALLING);
  attachInterrupt(digitalPinToInterrupt(sw2Pin), ISRsw2, FALLING);
  Serial.begin(115200);
  delay(600);                // wait for seial port to connect
  Serial.setTxTimeoutMs(10); // igore serial port errors if there is nothing connected

  // need to activate the INA219 and take a qiesent current reading with both ports turned off

  // Write SAO config to EEPROM
  Wire.begin(SDA_pin, SCL_pin, 100000);
  // maybe do a test here to see if the write to SAO board button switch is being held down while booting up?
  // If so, activate the WiFi to allow user to select the type of SAO board it is and write data to SAO Hat.

  //  Need to check both SAO ports to see whats plugged in
  if (MP.begin() == false)
  {
    Serial.println("COULD NOT CONNECT to PCA9546 i2C Bus Master IC");
  }

  channels = MP.channelCount();
  Serial.print("The i2C switch IC has:");
  Serial.print(MP.channelCount()); // how many i2C channels does this IC have?
  Serial.println(" switchable channels");
  // enable the two channels the board uses
  MP.enableChannel(0);
  MP.enableChannel(1);
  MP.disableChannel(2);
  MP.disableChannel(3);
  // Loop over the two channels we are using: SAO1 = channel1; SAO2 = channel0. Need to turn on power to each one before
  //  the Port becomes usable
  for (int chan = 0; chan < 2; chan++)
  {
    switch (chan)
    {
    case 0:
      // turn on SAO Port 2
      Serial.println("Port1");
      digitalWrite(PowerMOSFET1, HIGH); // turn off SAO port 1
      digitalWrite(PowerMOSFET2, LOW);  // turn on SAO port 2
      break;
    case 1:
      // turn on SAO Port 1
      Serial.println("Port2");
      digitalWrite(PowerMOSFET1, LOW);  // turn on SAO port 1
      digitalWrite(PowerMOSFET2, HIGH); // turn off SAO port 2
      break;
    default:
      // a fault has been found, switch off both SAO ports
      digitalWrite(PowerMOSFET1, HIGH); // turn off SAO port 1
      digitalWrite(PowerMOSFET2, HIGH); // turn off SAO port 2
      break;
    }
    Serial.print("Channel: ");
    MP.selectChannel(chan);
    Serial.print(chan);
    Serial.println(" has been selected");
    delay(100);

    num_led[chan] = readEEPROM(7); // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
    // const u8_t num_led = readEEPROM(7); // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
    //  ### NOTE: need to turn on relevant SAO Port before talking to the EEPROM. Need code for there  ####
    /*
    //char *SAOstring = "0001LR4,koala"; // 12 char long string
      char * SAOstring = "0002LR3,troublemaker";   // 19 char long string
     // char * SAOstring = "0003LR1,Kicon";  // 12 char long string
     Serial.println("Writing EEPROM Data");
      writeStringToEEPROM(0, SAOstring);
     Serial.println("Reading EEPROM Data: ");
     // Serial.println(readStringFromEEPROM(0));
     for (u8_t i = 0; i < 23; i++)
     {
       Serial.print(i);
       Serial.print("=");
       Serial.println(readEEPROM(i));
     }

     Serial.println(" ");
     Serial.println("Paused - remove EEPROM");
     while ((1))
     {
       // pause here
     }
   */
    Serial.print("Number of LEDs on SAO Port");
    Serial.print(chan);
    Serial.print(": ");
    Serial.println(num_led[chan]);
    if (num_led[chan] == 255)
    {
      // there might not be a SAO Hat on the bus?  It will report back 255.
      activePort[chan][0] = {0}; // is it active?
      activePort[chan][1] = {0}; // type of SAO board
      Serial.print("Port: ");
      Serial.print(chan);
      Serial.println(" is NOT active");
    }
    else
    {
      // must be an active i2C bus
      Serial.print("Port: ");
      Serial.print(chan);
      Serial.println(" is active!");
      if (skip_i2C == false)
      {
        Serial.print("SAO Device Number: ");
        target = readEEPROM(4) - 48; // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
        Serial.println(target);
        activePort[chan][0] = {1};      // is it active?
        activePort[chan][1] = {target}; // type of SAO board
      }
      else
      {
        target = 0;
      }
    }
  }
  for (u8_t i = 0; i < maxLEDs; i++)
  {
    // set all RGB LEDs to off
    leds[i] = CRGB(0, 0, 0);
    FastLED.show();
  }
  blinkUserLEDBlue(3000);
  Serial.println("Setup Finished");
}

void loop()
{
  u16_t heartMax = 40;
  u16_t heartMin = 5;

  for (int chan = 0; chan < 2; chan++)  // cycle through both i2C channels, ie: SAO Ports
  {
    switch (chan)
    {

    case 0:
      // turn on SAO Port 2
      Serial.println("Port1");
      digitalWrite(PowerMOSFET1, HIGH); // turn off SAO port 1
      digitalWrite(PowerMOSFET2, LOW);  // turn on SAO port 2
      break;
    case 1:
      // turn on SAO Port 1
      Serial.println("Port2");
      digitalWrite(PowerMOSFET1, LOW);  // turn on SAO port 1
      digitalWrite(PowerMOSFET2, HIGH); // turn off SAO port 2
      break;
    default:
      // a fault has been found, switch off both SAO ports
      digitalWrite(PowerMOSFET1, HIGH); // turn off SAO port 1
      digitalWrite(PowerMOSFET2, HIGH); // turn off SAO port 2
      break;
    }
    Serial.print("Channel: ");
    MP.selectChannel(chan);
    Serial.print(chan);
    Serial.print(" has been selected and has a: ");
    Serial.print(activePort[chan][1]);
    Serial.println(" plugged into it");
    delay(100);

    // ### NOTE: need to turn on relevant SAO Port before talking to the EEPROM. Need code for there  ####
    //           Maybe put the SAO Port GPIO for pins 5+6 into an array or Struct to make it easier address the SAO port.
    Serial.print("Skip Test: ");
    Serial.println(skip_i2C);
    /*
      if (skip_i2C == false)
      {
        Serial.print("SAO Device Number: ");
        target = readEEPROM(4) - 48; // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
        Serial.println(target);
      }
      else
      {
        target = 0;
      }
    */
    // target = 2;  // debug code - delete this line!
    switch (target)
    {
    case NONE:
      // nothing found - run the KiCon SZ23 code
      skip_i2C = true;
      Serial.println("Nothing found - run KiCon SAO hat");
      ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit brighter
      ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit brighter
      leds[0] = CRGB(35, 0, 0);    // red
      // leds[0] = CRGB::Red;
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 45, 0);    // green
      // leds[0] = CRGB::Green;
      FastLED.show();
      delay(1500);
      // leds[0] = CRGB::Blue;
      leds[0] = CRGB(0, 0, 35);    // blue
      FastLED.show();
      delay(1500);
      for (u8_t i = 0; i < 5; i++)
      {
        ledcWrite(ledChannel3, 2095); // turn on plain 0402 LEDs a bit brighter
        ledcWrite(ledChannel4, 2095); // turn on plain 0402 LEDs a bit brighter
        delay(150);
        ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit
        ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit
        delay(150);
      }
      break;
    case KOALA:
      // run the Koala code
      Serial.println("A Koala SAO hat has been found");
      // test sequence of LEDs + see how the first Koala works with  amix of SK6812 + 6028 LEDs
      //   seems one is RGB, the other is GRB + brightness varies?!
      FastLED.setBrightness(055);
      /*  Test each LED seperatly
      leds[0] = CRGB(9, 00, 0);  // D3 - LH Cheek
      leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
      leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
      leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
      leds[1] = CRGB(9, 0, 00);  // D4 - LH eye
      leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
      leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
      leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
      leds[2] = CRGB(9, 0, 00);  // D5 - RH eye
      leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
      leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
      leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
      leds[3] = CRGB(9, 00, 00); // D6 - RH cheek
      FastLED.show();
      delay(1500);
      */
      // normal Koala code
      leds[0] = CRGB(55, 00, 07); // D3 - LH Cheek
      leds[1] = CRGB(15, 07, 00); // D4 - LH eye
      leds[2] = CRGB(15, 07, 00); // D5 - RH eye
      leds[3] = CRGB(55, 00, 07); // D6 - RH cheek
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(55, 00, 07); // D3 - LH cheek
      leds[1] = CRGB(15, 05, 15); // D4 - LH eye
      leds[2] = CRGB(15, 05, 15); // D5 - RH eye
      leds[3] = CRGB(55, 00, 07); // D6 - RH cheek
      FastLED.show();
      delay(1500);

      for (u8_t i = 0; i < 5; i++)
      {
        ledcWrite(ledChannel3, 2095); // turn on plain 0402 LEDs a bit brighter
        ledcWrite(ledChannel4, 2095); // turn on plain 0402 LEDs a bit brighter
        delay(150);
        ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit
        ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit
        delay(150);
      }
      break;
    case TROUBLEMAKER:
      // run the TroubleMaker code
      Serial.println("A TroubleMaker SAO hat has been found");
      // ledcWrite(ledChannel1, 2095); // turn on plain 0402 LEDs a bit brighter
      leds[0] = CRGB(10, 010, 10); // left eye
      leds[1] = CRGB(10, 010, 10); // right eye
      FastLED.show();
      for (u8_t i = heartMin; i < heartMax; i++)
      {
        leds[2] = CRGB(i, 0, 0); // heart
        FastLED.show();
        delay(50);
      }
      for (u8_t i = heartMax; i > heartMin; i--)
      {
        leds[2] = CRGB(i, 0, 0); // heart
        FastLED.show();
        delay(50);
      }
      for (u8_t i = 0; i < 5; i++)
      {
        ledcWrite(ledChannel3, 2095); // turn on plain 0402 LEDs a bit brighter
        ledcWrite(ledChannel4, 2095); // turn on plain 0402 LEDs a bit brighter
        delay(150);
        ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit
        ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit
        delay(150);
      }
      break;
    case KICON23:
      // run the KiCon SZ23 code
      Serial.println("A KiCon SAO hat has been found");
      ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit
      ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit
      leds[0] = CRGB(35, 0, 0);
      // leds[0] = CRGB::Red;
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 45, 0);
      // leds[0] = CRGB::Green;
      FastLED.show();
      delay(1500);
      // leds[0] = CRGB::Blue;
      leds[0] = CRGB(0, 0, 35);
      FastLED.show();
      delay(1500);
      for (u8_t i = 0; i < 5; i++)
      {
        ledcWrite(ledChannel3, 2095); // turn on plain 0402 LEDs a bit brighter
        ledcWrite(ledChannel4, 2095); // turn on plain 0402 LEDs a bit brighter
        delay(150);
        ledcWrite(ledChannel3, 395); // turn on plain 0402 LEDs a bit
        ledcWrite(ledChannel4, 395); // turn on plain 0402 LEDs a bit
        delay(150);
      }
      break;
    default:
      // by default - run the KiCon 2023 code on the Port it's plugged into
      // run the KiCon SZ23 code
      skip_i2C = true;
      Serial.println("Nothing has been found - running KiCon SAO Hat code");
      ledcWrite(ledChannel3, 395); // turn on plain SAO1 0402 LEDs a bit
      ledcWrite(ledChannel4, 395); // turn on plain SAO2 0402 LEDs a bit
      leds[0] = CRGB(35, 0, 0);
      // leds[0] = CRGB::Red;
      FastLED.show();
      delay(1500);
      leds[0] = CRGB(0, 45, 0);
      // leds[0] = CRGB::Green;
      FastLED.show();
      delay(1500);
      // leds[0] = CRGB::Blue;
      leds[0] = CRGB(0, 0, 35);
      FastLED.show();
      delay(1500);
      for (u8_t i = 0; i < 5; i++)
      {
        ledcWrite(ledChannel3, 2095); // turn on plain SAO1 0402 LEDs a bit brighter
        ledcWrite(ledChannel4, 2095); // turn on plain SAO2 0402 LEDs a bit brighter
        delay(150);
        ledcWrite(ledChannel3, 395); // turn on plain SAO1 0402 LEDs a bit
        ledcWrite(ledChannel4, 395); // turn on plain SAO2 0402 LEDs a bit
        delay(150);
      }
      break;
    }
  }
  // rainbow_wave(35);
  // toggleGreen();
  blinkUserLEDBlue(3000);
}
