#include <Arduino.h>
#include <NeoPixelBus.h>        // RGB LED driver
#include <Wire.h>               // i2C driver
#include <Bonezegei_SSD1306.h>  // OLED 128x64px display driver


/* Raspberry Pico RP2040 SAO Controller PCB.  For SAO Controller PCB Versions 0.2, 1.0 and 1.1
This is meant to be simple code to get people playing with SAO boards.  
See my SAO project hardware and software repo here:  https://gitlab.com/parkview/Simple-Add-On_Tester_PCBs

Note: using when using Arduino IDE 2.3.3, set Arduino Board type to:  Generic RP2040

Arduino Library's: 
https://github.com/Makuna/NeoPixelBus wiki: https://github.com/Makuna/NeoPixelBus/wiki for the RGB LEDs
https://github.com/bonezegei/Bonezegei_SSD1306   OLED display

This uses simple sequentual code.  Idealy these should be using something like a FreeRTOS system to make it all non-blocking.

So far I have tested 3 types of RGB LEDs using this RGB LED Sequence:
WS2812:  RGB
SK6812:  GRB??
6028RGB: BRG

GPIO25 = User Button switch

Terminology:
  Badge = base controller that has a microprocessor (MCU)
  SAO   = a small 'Simple Add On' PCB that plugs into a 'Badge'

Example code by Parkview 2024 - all Open Source Software  

*/

// Demonstrating the use of the four channels
NeoPixelBus<NeoBgrFeature, Rp2040x4Pio1Ws2812xMethod> strip1(10, 7);  // I can have up to 10 RGB LEDs running via GPIO pin 7
// NeoPixelBus<NeoBgrFeature, Rp2040x4NWs2811Method> strip4(4, 7);             // note: older WS2811 and longer strip
// NeoPixelBus<NeoGrbFeature, Rp2040x4Pio1NWs2812xMethod> strip4(4, 7);        // note: modern WS2812 with letter like WS2812b
// NeoPixelBus<NeoGrbFeature, Rp2040x4Pio1Ws2812xInvertedMethod> strip4(4, 7); // note: inverted
// NeoPixelBus<NeoGrbwFeature, Rp2040x4Pio1Sk6812Method> strip4(4, 7);         // note: RGBW and Sk6812 and smaller strip

Bonezegei_SSD1306 oled(128, 64);  // for the SAO OLED Display

const char *version = "V1.1a";
const uint8_t pwrLEDPin = 12;   // Controller/Badge D2 PWR LED GPIO pin
const uint8_t userLEDPin = 15;  // Controller/Badge D3 User LED GPIO pin
const uint8_t userSWPin = 25;   // Controller/Badge User switch GPIO pin
const uint8_t saoPin7 = 7;      // SAO hat/boards RGB WS2812 LED GPIO pin
const uint8_t saoPin10 = 10;    // SAO hat/boards plain LED GPIO pin
const uint8_t i2cSCLPin = 5;    // SAO hat/boards i2C Clock GPIO pin
const uint8_t i2cSDAPin = 4;    // SAO hat/boards i2C Data GPIO pin
#define PIN_WIRE_SDA 4
#define PIN_WIRE_SCL 5
const uint8_t EEPROM_I2C_ADDRESS = 0x50;            // generic 256 byte EEPROM = 80D
uint8_t address = 0;                                // initial EEPROM Address. Examples: value = EEPROM.read(address); EEPROM.update(address, val); EEPROM.length())
bool skip_i2C = false;                              // ignore i2C if no device is found
const uint8_t maxLEDs = 15;                         // due to the Power 133mA limit, this limits the max number of RGB LEDs we can use. Really it should be around 2(2 x 60mA = 120mA!) at full brightness
uint8_t num_led = 0;                                // tracks the number of RGB LEDs on a SAO Hat (read from EEPROM) on that SAO Port
uint8_t activePort[2][2] = { { 0, 0 }, { 0, 0 } };  // tracks if a SAO port is in use or not, and the type of SAO (Target) that installed on it
uint8_t target = 0;                                 // records the type of SAO Hat.
const uint16_t frequency = 5000;                    // LED blink frequency
const uint16_t range = 1024;                        // duty cylce range
const uint16_t resolution = 1024;                   // duty cycle resolution
bool switchPressed = false;                         // keeps the User switch State
const u_int8_t heartMin = 1;
const u_int8_t heartMax = 55;
const uint8_t minLEDbrightness = 20;   // minimum brightness for the plain SAO LEDs
const uint8_t plainLEDBrightness = 4;  // User and PWR LED brightness. 4 is a nice brightness for the eyes
#define colSat = 40;                   // colour satuation
const uint8_t NUM_LEDS = 9;
uint8_t j = 0;
/*
// this doesn't work - bad syntax
RgbColor red(0, colSat, 0);
//RgbColor red(colorSaturation, 0, 0);
RgbColor cyan(0,colSat, colSat);
RgbColor yellow(colSat,0,colSat);
RgbColor blue(0,0,colSat);
RgbColor red(colSat,0,0);
RgbColor green(0,colSat,0);
RgbColor pink(colSat, colSat,0);
*/
enum SAOType {
  NONE,
  KOALA,
  TROUBLEMAKER,
  KICON23,
  KICON24,
  MAKEIT,
  LINUXAUSTRALIA,
  COMET,
  OLEDDISPLAY,
  SUSTECH24
};  // tracks the type of SAO board it is - u8_t = 0,1,2,3...

void writeEEPROM(uint8_t address, uint8_t val);
uint8_t readEEPROM(uint8_t address);

void switchPress()
// IRAM_ATTR void switchPress()
{
  // the button switch interrupt has been called
  switchPressed = true;
}

void writeStringToEEPROM(uint8_t addrOffset, const String &strToWrite) {
  byte len = strToWrite.length();
  writeEEPROM(addrOffset, len);
  for (int i = 0; i < len; i++) {
    writeEEPROM(addrOffset + 1 + i, strToWrite[i]);
    Serial.println(strToWrite[i]);
  }
}

String readStringFromEEPROM(uint8_t addrOffset) {
  int newStrLen = readEEPROM(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++) {
    data[i] = readEEPROM(addrOffset + 1 + i);
  }
  data[newStrLen] = '\0';  // !!! NOTE !!! Remove the space between the slash "/" and "0" (I've added a space because otherwise there is a display bug)
  return String(data);
}
// Function to write to EEPROOM
void writeEEPROM(uint8_t address, uint8_t val) {
  // Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((uint8_t)(address));  // AT24C02 only needs one byte address
  Wire.write(val);                 // Send data to be stored
  Wire.endTransmission();          // End the transmission
  delay(5);                        //  Add 5ms delay for EEPROM
}

// Function to read from EEPROM
uint8_t readEEPROM(uint8_t address) {
  uint8_t rcvData = 0xFF;  // Define byte for received data
  //  Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((uint8_t)(address));  // AT24C02 only needs one byte address
  Wire.endTransmission(false);     // should report back a 0 for success
  delayMicroseconds(40);
  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);  // Request one byte of data at current memory address
  // Read the data and assign to variable
  if (Wire.available()) {
    rcvData = Wire.read();
  }
  delayMicroseconds(40);
  // Return the data as function output
  return rcvData;
}

void toggleGreen(uint8_t blinks) {
  for (uint8_t j = 0; j < blinks; j++) {
    // blinks the Badge User LED a few times
    delay(200);
    analogWrite(userLEDPin, 25);  // Badge boards user LED GPIO pin
    delay(200);
    analogWrite(userLEDPin, plainLEDBrightness);  // Badge boards User LED GPIO pin
  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(pwrLEDPin, OUTPUT);                        // set the Boards Power LED GPIO to Output
  pinMode(userLEDPin, OUTPUT);                       // set the Boards User LED GPIO to Output
  pinMode(saoPin7, OUTPUT);                          // boards RGB LED GPIO pin
  pinMode(saoPin10, OUTPUT);                         // boards LED GPIO pin
  pinMode(PIN_WIRE_SDA, OUTPUT);                     // boards i2C Data pin
  pinMode(PIN_WIRE_SCL, OUTPUT);                     // boards i2C Clock pin
  pinMode(userSWPin, INPUT);                         // boards LED GPIO pin
  analogWrite(pwrLEDPin, plainLEDBrightness);        // light up the Badge's Power LED
  analogWrite(userLEDPin, plainLEDBrightness + 10);  // light up the Badge's User LED
  Serial.begin(115200);                              // turn on the serial port
  delay(900);                                        // allow time for the serial port to open up
  Wire.begin();                                      // turn on the i2C port
  strip1.Begin();                                    // get the RGB LED routine started
  num_led = readEEPROM(7);                           // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
                                                     // const u8_t num_led = readEEPROM(7); // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
                                                     //  ### NOTE: need to turn on relevant SAO Port before talking to the EEPROM. Need code for there  ####
  //  SAOString = [numBytes] = unique board number; [L] = L= has static LEDs present; R[num] = number of RGB LEDs, ie: 0, 1,2,3,4...; name of board
  /*
  // NOTE: There is now a seperate program to write data to a SAO boards EEPROM
  //char *SAOstring = "0001LR4,koala";           // 12 char long string
    char * SAOstring = "0002LR3,troublemaker";   // 19 char long string
   // char * SAOstring = "0003LR1,Kicon";        // 12 char long string
   Serial.println("Writing EEPROM Data");
   writeStringToEEPROM(0, SAOstring);
   Serial.println("Reading EEPROM Data: ");
   // Serial.println(readStringFromEEPROM(0));
   for (u8_t i = 0; i < 23; i++)
   {
     Serial.print(i);
     Serial.print("=");
     Serial.println(readEEPROM(i));
   }
   Serial.println(" ");
   Serial.println("Paused - remove EEPROM");
   while ((1))
   {
     // pause here
   }
 */
  if (skip_i2C == false) {
    Serial.print("SAO Device Number: ");
    target = readEEPROM(4) - 48;  // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
    Serial.println(target);
    // activePort[chan][0] = {1};      // is it active?
    // activePort[chan][1] = {target}; // type of SAO board
  } else {
    target = 0;
  }
  // set all RGB LEds to black/off
  for (uint8_t i = 0; i < maxLEDs; i++) {
    // set all RGB LEDs to off
    strip1.SetPixelColor(i, RgbColor(0, 0, 0));  // left eye
  }
  strip1.Show();  // update RGB  LEDs, ie: turn them off
  attachInterrupt(digitalPinToInterrupt(userSWPin), switchPress, RISING);
  oled.begin();
  oled.clear();
  //oled.drawText(10, 0, "Hello World", 1, arial_8ptBitmaps, arial_8ptDescriptors);
  oled.draw();  //update the screen
  Serial.print("Setup Finished for: ");
  Serial.println(version);
}

void loop() {
  switch (target) {
    case NONE:
      // nothing found - run the KiCon SZ23 code
      skip_i2C = true;
      Serial.println("Nothing found - run KiCon SAO");
      analogWrite(saoPin10, minLEDbrightness);     // turn on plain 0402 LEDs a bit brighter
      strip1.SetPixelColor(0, RgbColor(0, 8, 0));  // 6028RGB LED - Red
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(35, 0, 0));  // 6028RGB LED - Blue
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(0, 0, 35));  // 6028RGB LED - Green
      strip1.Show();
      delay(1500);
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(250);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LEDs
        delay(150);
      }
      break;
    case KOALA:
      // run the Koala code
      Serial.println("A Koala SAO has been found");
      // test sequence of LEDs + see how the first Koala works with  amix of SK6812 + 6028RGB LEDs
      //   seems one is RGB, the other is GRB + brightness varies?!
      /*  Test each LED separately via FastLED - example code!
    leds[0] = CRGB(9, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(9, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(9, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(9, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    */
      // normal Koala code
      strip1.SetPixelColor(0, RgbColor(70, 70, 100));   // left eye
      strip1.SetPixelColor(1, RgbColor(00, 150, 100));  // left cheek
      strip1.SetPixelColor(2, RgbColor(00, 150, 100));  // right cheek
      strip1.SetPixelColor(3, RgbColor(70, 70, 100));   // right eye
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(70, 70, 100));  // Left Eye?
      strip1.SetPixelColor(1, RgbColor(70, 150, 8));   // Left cheek
      strip1.SetPixelColor(2, RgbColor(70, 150, 8));   // right cheek
      strip1.SetPixelColor(3, RgbColor(70, 70, 100));  // right eye
      strip1.Show();
      delay(1500);
      for (uint8_t i = 0; i < 5; i++) {
        // blink the toe LEDs
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(150);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LEDs
        delay(150);
      }
      break;
    case TROUBLEMAKER:
      // run the TroubleMaker code
      Serial.println("A TroubleMaker SAO has been found");
      strip1.SetPixelColor(0, RgbColor(10, 10, 10));  // left eye
      strip1.SetPixelColor(1, RgbColor(10, 10, 10));  // right eye
      strip1.Show();
      for (uint8_t i = heartMin; i < heartMax; i++) {
        strip1.SetPixelColor(2, RgbColor(i, i, i));  // breath the heart up
        strip1.Show();
        delay(10);
      }
      for (uint8_t i = heartMax; i > heartMin; i--) {
        strip1.SetPixelColor(2, RgbColor(i, i, i));  //breath the heart down
        strip1.Show();
        delay(10);
      }
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(150);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LEDs
        delay(150);
      }
      break;
    case KICON23:
      // run the KiCon SZ23 code
      Serial.println("A KiCon 23 SAO has been found");
      analogWrite(saoPin10, 200);                   // turn on plain 0402 LEDs a bit brighter
      strip1.SetPixelColor(0, RgbColor(0, 35, 0));  // 6028RGB LED: Red
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(35, 0, 0));  // 6028RGB LED: Blue
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(0, 0, 35));  // 6028RGB LED: Green
      strip1.Show();
      delay(1500);
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(150);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LEDs
        delay(150);
      }
      break;
    case KICON24:
      // run the KiCon SZ23 code
      Serial.println("A KiCon 24 SAO has been found");
      analogWrite(saoPin10, 200);                  // turn on plain 0402 LEDs a bit brighter
      strip1.SetPixelColor(0, RgbColor(0, 8, 0));  // 6028RGB LED: Red
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(35, 0, 0));  // 6028RGB LED: Blue
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(0, 0, 35));  // 6028RGB LED: Green
      strip1.Show();
      delay(1500);
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LED a bit brighter
        delay(150);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LED
        delay(150);
      }
      break;
    case MAKEIT:
      // run the Mick Bennitt Make-It Space code.  This SAO has 2 RGB LEDs and 1 plain LEDs
      Serial.println("A Make-It Space SAO has been found");
      strip1.SetPixelColor(0, RgbColor(0, 35, 35));  //
      strip1.SetPixelColor(1, RgbColor(35, 0, 0));   //
      strip1.Show();
      delay(800);
      strip1.SetPixelColor(0, RgbColor(35, 35, 0));  //
      strip1.SetPixelColor(1, RgbColor(35, 0, 35));  //
      strip1.Show();
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(150);
        analogWrite(saoPin10, minLEDbrightness);  // turn down the plain 0402 LEDs
        delay(150);
      }
      delay(800);
      break;
    case LINUXAUSTRALIA:
      // run the Linux Australia code.  This SAO has 5 RGB LEDs and no plain LEDs
      linuxAustralia();
      break;
    case COMET:
      // run the Comet code.  This SAO has 1 RGB LED and 36-ish plain LEDs run via a LED Driver IC
      Serial.println("A Comet SAO has been found");
      strip1.SetPixelColor(0, RgbColor(0, 35, 0));  // single RGB LED
      strip1.Show();
      // more code needed here to drive the 36 LED driver IC...
      delay(1500);
      break;
    case OLEDDISPLAY:
      // run the OLED 128x64 Display code.  This SAO has 1 RGB LEDs and no plain LEDs
      Serial.println("A OLED Display SAO has been found");
      strip1.SetPixelColor(0, RgbColor(0, 35, 0));  // RGB Back light LED
      strip1.Show();
      // Write something to the OLED Display
      oled.clear();
      oled.draw();  //update the screen
      //text starts at top LH side and goes up!!
      oled.drawText(3, 11, "Arial8", oled.Font_Arial8);
      oled.drawText(3, 22, "Arial10", oled.Font_Arial10);
      oled.drawText(3, 34, "Arial12", oled.Font_Arial12);
      oled.drawText(3, 49, "Arial14", oled.Font_Arial14);
      oled.draw();  //update the screen
      delay(1500);
      break;
    case SUSTECH24:
      // This SAO has 9 BRG LEDs and no plain LEDs
      Serial.println("A SUSTECH 24 SAO has been found");
      //j=0;
      // display SUSTech logo on LEDs
      //uint8_t i = 0;
      //delay(1500);
      for (uint8_t i = 10; i < 50; i += 2) {
        //   ChangeColor();
        pulseSustechLEDs(i);
        delay(40);
      }
      delay(200);
      for (uint8_t i = 50; i > 10; i -= 2) {
        //   ChangeColor();
        pulseSustechLEDs(i);
        delay(40);
      }
      delay(800);
      // display moving rainbow for 2 seconds
      //delay(1500);  //remove this later when rainbow is in place
      break;
    default:
      // nothing found, run the default KiCon 2023 code on the Port it's plugged into
      // run the KiCon SZ23 code
      skip_i2C = true;
      Serial.println("Nothing has been found - running KiCon SAO code");
      analogWrite(saoPin10, 50);                   // turn on plain 0402 LEDs a bit brighter
      strip1.SetPixelColor(0, RgbColor(0, 8, 0));  //  6028RGB LED: Red
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(45, 0, 0));  //  6028RGB LED: Blue
      strip1.Show();
      delay(1500);
      strip1.SetPixelColor(0, RgbColor(0, 0, 35));  //  6028RGB LED: Green
      strip1.Show();
      delay(1500);
      for (uint8_t i = 0; i < 5; i++) {
        analogWrite(saoPin10, 150);  // turn on plain 0402 LEDs a bit brighter
        delay(250);
        analogWrite(saoPin10, minLEDbrightness);  // turn on plain 0402 LEDs a bit brighter
        delay(150);
      }
      break;
  }
  toggleGreen(3);  // blink the Badge User LED a few times to let people know something is happening
}

void linuxAustralia(void) {
  Serial.println("A Linux Australia SAO has been found");
  strip1.SetPixelColor(4, RgbColor(0, 0, 35));   //
  strip1.SetPixelColor(3, RgbColor(0, 35, 0));   //
  strip1.SetPixelColor(2, RgbColor(0, 35, 35));  //
  strip1.SetPixelColor(1, RgbColor(35, 0, 35));  //
  strip1.SetPixelColor(0, RgbColor(35, 35, 0));  //
  strip1.Show();
  delay(700);
  strip1.SetPixelColor(0, RgbColor(35, 0, 0));   //
  strip1.SetPixelColor(1, RgbColor(0, 35, 0));   //
  strip1.SetPixelColor(2, RgbColor(0, 35, 35));  //
  strip1.SetPixelColor(3, RgbColor(35, 0, 35));  //
  strip1.SetPixelColor(4, RgbColor(35, 35, 0));  //
  strip1.Show();
  Serial.println(num_led - 48);  // number of RGB LEDs on the SAO
  delay(700);
}

void pulseSustechLEDs(uint8_t ledBrightness) {
  strip1.SetPixelColor(0, RgbColor(ledBrightness, ledBrightness, 00));                 // RGB light LED (Top Left) [Pink]
  strip1.SetPixelColor(1, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED [yellow]
  strip1.SetPixelColor(2, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED (Top Right)
  strip1.SetPixelColor(3, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED
  strip1.SetPixelColor(4, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED
  strip1.SetPixelColor(5, RgbColor(ledBrightness, 00, 00));                            // RGB light LED [Blue]
  strip1.SetPixelColor(6, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED (Bottom Left)
  strip1.SetPixelColor(7, RgbColor(ledBrightness - 5, ledBrightness, ledBrightness));  // RGB light LED [white]
  strip1.SetPixelColor(8, RgbColor(00, ledBrightness, ledBrightness));                 // RGB light LED (Bottom Right)
  strip1.Show();
}

void ChangeColor() {

  for (uint16_t i = 0; i < NUM_LEDS; i++) {
    // generate a value between 0~255 according to the position of the pixel
    // along the strip
    uint16_t pos = ((i * 256 / NUM_LEDS) + j) & 0xFF;
    // calculate the color for the ith pixel
    RgbColor color = Wheel(pos);
    // set the color of the ith pixel
    strip1.SetPixelColor(i, color);
  }
  strip1.Show();

  j++;
  if (j == 255) {
    j = 0;
  }
}
RgbColor Wheel(uint8_t WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return RgbColor(255 - WheelPos * 3, 0, WheelPos * 3);
  } else if (WheelPos < 170) {
    WheelPos -= 85;
    return RgbColor(0, WheelPos * 3, 255 - WheelPos * 3);
  } else {
    WheelPos -= 170;
    return RgbColor(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
}
