#include <Arduino.h>
#include <NeoPixelBus.h>
#include <Wire.h>

/* Raspberry Pico RP2040 SAO Controller PCB.  For PCB Version 0.2

GPIO25 = User Button switch

Terminology:
  Badge = base controller that has a microprocessor (MCU)
  SAO Hat = the small PCB that plugs into a 'Badge'

*/

// Demonstrating the use of the four channels
NeoPixelBus<NeoBgrFeature, Rp2040x4Pio1Ws2812xMethod> strip1(4, 7);
// NeoPixelBus<NeoBgrFeature, Rp2040x4NWs2811Method> strip4(4, 7); // note: older WS2811 and longer strip
// NeoPixelBus<NeoGrbFeature, Rp2040x4Pio1NWs2812xMethod> strip4(4, 7); // note: modern WS2812 with letter like WS2812b
// NeoPixelBus<NeoGrbFeature, Rp2040x4Pio1Ws2812xInvertedMethod> strip4(4, 7); // note: inverted
// NeoPixelBus<NeoGrbwFeature, Rp2040x4Pio1Sk6812Method> strip4(4, 7); // note: RGBW and Sk6812 and smaller strip
// NeoPixelBus<NeoBgrFeature, Rp2040x4

const char *version = "V0.2a";
const uint8_t pwrLEDPin = 12;    // Badge D2 PWR LED GPIO pin
const uint8_t userLEDPin = 15;   // Badge D3 User LED GPIO pin
const uint8_t saoPin7 = 7;       // SAO boards RGB WS2812 LED GPIO pin
const uint8_t saoPin10 = 10;     // SAO boards plain LED GPIO pin
const uint8_t i2cSCLPin = 5;     // SAO boards LED GPIO pin
const uint8_t i2cSDAPin = 4;     // SAO boards LED GPIO pin
#define PIN_WIRE_SDA 4
#define PIN_WIRE_SCL 5
const uint8_t userSWPin = 25;                 // boards User switch GPIO pin
const uint8_t EEPROM_I2C_ADDRESS = 0x50;      // EEPROM = 80D
uint8_t address = 0;                          // initial EEPROM Address. Examples: value = EEPROM.read(address); EEPROM.update(address, val); EEPROM.length())
bool skip_i2C = false;                        // ignore i2C if no device is found
const uint8_t maxLEDs = 15;                   // due to the Power 133mA limit, this limits the max number of RGB LEDs we can use. Really it should be around 2(2 x 60mA = 120mA!) at full brightness
uint8_t num_led = 0;                          // tracks the number of RGB LEDs on a SAO Hat (read from EEPROM) on that SAO Port
uint8_t activePort[2][2] = {{0, 0}, {0, 0}};  // tracks if a SAO port is in use or not, and the type of SAO (Target) that installed on it
uint8_t target = 0;                           // records the type of SAO Hat.
const uint16_t frequency = 5000;              // LED blink frequency
const uint16_t range = 1024;                  // duty cylce range
const uint16_t resolution = 1024;             // duty cycle resolution
bool switchPressed = false;                   // keeps the User switch State
const u_int8_t heartMin = 1;
const u_int8_t heartMax = 3;

enum SAOType
{
  NONE,
  KOALA,
  TROUBLEMAKER,
  KICON23,
  KICON24,
  MAKEIT,
  LINUXAUSTRALIA,
  COMET
}; // tracks the type of SAO board it is - u8_t = 0,1,2,3

void writeEEPROM(uint8_t address, uint8_t val);
uint8_t readEEPROM(uint8_t address);

void switchPress()
// IRAM_ATTR void switchPress()
{
  // the button switch interrupt has been called
  switchPressed = true;
}

void writeStringToEEPROM(int addrOffset, const String &strToWrite)
{
  byte len = strToWrite.length();
  writeEEPROM(addrOffset, len);
  for (int i = 0; i < len; i++)
  {
    writeEEPROM(addrOffset + 1 + i, strToWrite[i]);
    Serial.println(strToWrite[i]);
  }
}

String readStringFromEEPROM(int addrOffset)
{
  int newStrLen = readEEPROM(addrOffset);
  char data[newStrLen + 1];
  for (int i = 0; i < newStrLen; i++)
  {
    data[i] = readEEPROM(addrOffset + 1 + i);
  }
  data[newStrLen] = '\0'; // !!! NOTE !!! Remove the space between the slash "/" and "0" (I've added a space because otherwise there is a display bug)
  return String(data);
}

// Function to write to EEPROOM
void writeEEPROM(uint8_t address, uint8_t val)
{
  // Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((uint8_t)(address)); // AT24C02 only needs one byte address
  // Send data to be stored
  Wire.write(val);
  // End the transmission
  Wire.endTransmission();
  //  Add 5ms delay for EEPROM
  delay(5);
}

// Function to read from EEPROM
uint8_t readEEPROM(uint8_t address)
{
  // Define byte for received data
  uint8_t rcvData = 0xFF;
  // byte rcvData = 0xFF;
  //  Begin transmission to I2C EEPROM
  Wire.beginTransmission(EEPROM_I2C_ADDRESS);
  Wire.write((uint8_t)(address)); // AT24C02 only needs one byte address
                                  // End the transmission
                                  // Serial.print("i2C Write: ");
  // Wire.endTransmission(); // should report back a 0 for success
  Wire.endTransmission(false); // should report back a 0 for success
  delayMicroseconds(40);
  // Request one byte of data at current memory address
  Wire.requestFrom(EEPROM_I2C_ADDRESS, 1);
  // Read the data and assign to variable
  if (Wire.available())
  {
    rcvData = Wire.read();
  }
  delayMicroseconds(40);
  // Return the data as function output
  return rcvData;
}

void toggleGreen(uint8_t blinks)
{
  for (uint8_t j = 0; j < blinks; j++)
  {
    // blink user LED a few times
    delay(200);
    analogWrite(userLEDPin, 0);  // Badge boards user LED GPIO pin
    delay(200);
    analogWrite(userLEDPin, 10); // Badge boards User LED GPIO pin
  }
}

void setup()
{
  // put your setup code here, to run once:
  pinMode(pwrLEDPin, OUTPUT);
  pinMode(userLEDPin, OUTPUT);
  pinMode(saoPin7, OUTPUT);       // boards RGB LED GPIO pin
  pinMode(saoPin10, OUTPUT);      // boards LED GPIO pin
  pinMode(i2cSCLPin, OUTPUT);     // boards LED GPIO pin
  pinMode(i2cSDAPin, OUTPUT);     // boards LED GPIO pin
  pinMode(userSWPin, INPUT);      // boards LED GPIO pin
  analogWrite(pwrLEDPin, 10); 
  analogWrite(userLEDPin, 10); 
  //digitalWrite(pwrLEDPin, HIGH);  // turn on run/power indicator LED
  //digitalWrite(userLEDPin, HIGH); // turn on run/power indicator LED
  // analogWriteFreq(frequency);
  // analogWriteRange(range);
  //analogWriteResolution(resolution); // don't activate this line, as it locks up the RPi!!!!
  // const uint8_t i2cSCLPin = 5; // boards LED GPIO pin
  // const uint8_t i2cSDAPin = 4; // boards LED GPIO pin
  //  Wire.begin();
  //  Wire.begin(i2cSDAPin, i2cSCLPin);

  Serial.begin(115200);
  delay(900);
  Wire.begin();
  strip1.Begin();
  num_led = readEEPROM(7); // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
                           // const u8_t num_led = readEEPROM(7); // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
                           //  ### NOTE: need to turn on relevant SAO Port before talking to the EEPROM. Need code for there  ####

  //  SAOString = [numBytes] = unique board number; [L] = L= has static LEDs present; R[num] = number of RGB LEDs, ie: 0, 1,2,3,4...; name of board
  /*
  //char *SAOstring = "0001LR4,koala"; // 12 char long string
    char * SAOstring = "0002LR3,troublemaker";   // 19 char long string
   // char * SAOstring = "0003LR1,Kicon";  // 12 char long string
   Serial.println("Writing EEPROM Data");
    writeStringToEEPROM(0, SAOstring);
   Serial.println("Reading EEPROM Data: ");
   // Serial.println(readStringFromEEPROM(0));
   for (u8_t i = 0; i < 23; i++)
   {
     Serial.print(i);
     Serial.print("=");
     Serial.println(readEEPROM(i));
   }
   Serial.println(" ");
   Serial.println("Paused - remove EEPROM");
   while ((1))
   {
     // pause here
   }
 */

  if (skip_i2C == false)
  {
    Serial.print("SAO Device Number: ");
    target = readEEPROM(4) - 48; // need a test here in case the SAO has no i2C IC on it. Otherwise we get a read error.
    Serial.println(target);
    // activePort[chan][0] = {1};      // is it active?
    // activePort[chan][1] = {target}; // type of SAO board
  }
  else
  {
    target = 0;
  }
  // set all RGB LEds to black/off
  for (uint8_t i = 0; i < maxLEDs; i++)
  {
    // set all RGB LEDs to off
    strip1.SetPixelColor(i, RgbColor(0, 0, 0)); // left eye
  }
  strip1.Show();
  attachInterrupt(digitalPinToInterrupt(userSWPin), switchPress, RISING);
  // blinkUserLEDBlue(3000);
  Serial.println("Setup Finished");
}

void loop()
{
  switch (target)
  {
  case NONE:
    // nothing found - run the KiCon SZ23 code
    skip_i2C = true;
    Serial.println("Nothing found - run KiCon SAO hat");
    analogWrite(saoPin10, 20);                  // turn on plain 0402 LEDs a bit brighter
    strip1.SetPixelColor(0, RgbColor(0, 35, 0)); // left eye
    // leds[0] = CRGB::Red;
    strip1.Show();
    delay(1500);
    strip1.SetPixelColor(0, RgbColor(45, 0, 0)); // left eye
    // leds[0] = CRGB::Green;
    strip1.Show();
    delay(1500);
    // leds[0] = CRGB::Blue;
    strip1.SetPixelColor(0, RgbColor(0, 0, 35)); // left eye
    strip1.Show();
    delay(1500);
    for (uint8_t i = 0; i < 5; i++)
    {
      analogWrite(saoPin10, 200); // turn on plain 0402 LEDs a bit brighter
      delay(250);
      analogWrite(saoPin10, 20); // turn on plain 0402 LEDs a bit brighter
      delay(150);
    }
    break;
  case KOALA:
    // run the Koala code
    Serial.println("A Koala SAO hat has been found");
    // test sequence of LEDs + see how the first Koala works with  amix of SK6812 + 6028 LEDs
    //   seems one is RGB, the other is GRB + brightness varies?!
    // FastLED.setBrightness(055);
    /*  Test each LED seperatly
    leds[0] = CRGB(9, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(9, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(9, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(0, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    leds[0] = CRGB(0, 00, 0);  // D3 - LH Cheek
    leds[1] = CRGB(0, 0, 00);  // D4 - LH eye
    leds[2] = CRGB(0, 0, 00);  // D5 - RH eye
    leds[3] = CRGB(9, 00, 00); // D6 - RH cheek
    FastLED.show();
    delay(1500);
    */
    // normal Koala code
    strip1.SetPixelColor(0, RgbColor(70, 70, 100));  // left eye
    strip1.SetPixelColor(1, RgbColor(00, 150, 100)); // left cheek
    strip1.SetPixelColor(2, RgbColor(00, 150, 100)); // right cheek
    strip1.SetPixelColor(3, RgbColor(70, 70, 100));  // right eye
    strip1.Show();
    delay(1500);
    strip1.SetPixelColor(0, RgbColor(70, 70, 100)); // Left Eye?
    strip1.SetPixelColor(1, RgbColor(70, 150, 8));  // Left cheek
    strip1.SetPixelColor(2, RgbColor(70, 150, 8));  // right cheek
    strip1.SetPixelColor(3, RgbColor(70, 70, 100)); // right eye
    strip1.Show();
    delay(1500);

    for (uint8_t i = 0; i < 5; i++)
    {
      analogWrite(saoPin10, 500); // turn on plain 0402 LEDs a bit brighter
      delay(150);
      analogWrite(saoPin10, 200); // turn on plain 0402 LEDs a bit brighter
      delay(150);
    }
    break;
  case TROUBLEMAKER:
    // run the TroubleMaker code
    Serial.println("A TroubleMaker SAO hat has been found");
    // ledcWrite(ledChannel1, 2095); // turn on plain 0402 LEDs a bit brighter
    // analogWrite(ledPin, 200);    // turn on plain 0402 LEDs a bit brighter
    strip1.SetPixelColor(0, RgbColor(10, 010, 10)); // right cheek
    strip1.SetPixelColor(1, RgbColor(00, 010, 10)); // right eye
    strip1.Show();
    for (uint8_t i = heartMin; i < heartMax; i++)
    {
      strip1.SetPixelColor(0, RgbColor(0, i, 10)); // right cheek
      strip1.Show();
      delay(50);
    }
    for (uint8_t i = heartMax; i > heartMin; i--)
    {
      strip1.SetPixelColor(2, RgbColor(0, i, 10)); // right cheek
      strip1.Show();
      delay(50);
    }
    for (uint8_t i = 0; i < 5; i++)
    {
      analogWrite(saoPin10, 500); // turn on plain 0402 LEDs a bit brighter
      delay(150);
      analogWrite(saoPin10, 200); // turn on plain 0402 LEDs a bit brighter
      delay(150);
    }
    break;
  case KICON23:
    // run the KiCon SZ23 code
    Serial.println("A KiCon SAO hat has been found");
    analogWrite(saoPin10, 200);                  // turn on plain 0402 LEDs a bit brighter
    strip1.SetPixelColor(0, RgbColor(0, 35, 0)); //
    // leds[0] = CRGB::Red;
    strip1.Show();
    delay(1500);
    strip1.SetPixelColor(0, RgbColor(35, 0, 0)); //
    // leds[0] = CRGB::Green;
    strip1.Show();
    delay(1500);
    // leds[0] = CRGB::Blue;
    strip1.SetPixelColor(0, RgbColor(0, 0, 35)); //
    strip1.Show();
    delay(1500);
    for (uint8_t i = 0; i < 5; i++)
    {
      analogWrite(saoPin10, 500); // turn on plain 0402 LEDs a bit brighter
      delay(150);
      analogWrite(saoPin10, 200); // turn on plain 0402 LEDs a bit brighter
      delay(150);
    }
    break;
  default:
    // by default - run the KiCon 2023 code on the Port it's plugged into
    // run the KiCon SZ23 code
    skip_i2C = true;
    Serial.println("Nothing has been found - running KiCon SAO Hat code");
    analogWrite(saoPin10, 10);                  // turn on plain 0402 LEDs a bit brighter
    strip1.SetPixelColor(0, RgbColor(0, 35, 0)); //
    // leds[0] = CRGB::Red;
    strip1.Show();
    delay(1500);
    strip1.SetPixelColor(0, RgbColor(45, 0, 0)); //
    // leds[0] = CRGB::Green;
    strip1.Show();
    delay(1500);
    // leds[0] = CRGB::Blue;
    strip1.SetPixelColor(0, RgbColor(0, 0, 35)); //
    strip1.Show();
    delay(1500);
    for (uint8_t i = 0; i < 5; i++)
    {
      analogWrite(saoPin10, 200); // turn on plain 0402 LEDs a bit brighter
      delay(250);
      analogWrite(saoPin10, 10); // turn on plain 0402 LEDs a bit brighter
      delay(150);
    }
    break;
  }
  // blinkUserLEDBlue(3000);
  toggleGreen(3); // blink the onboard LED a few times
}
